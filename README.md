# Simple Cite Cloud add-in
The Simple Cite Cloud add-in brings citation functionality to Confluence.

## License
Please refer to our [Source code license agreement](https://purde-software.atlassian.net/wiki/spaces/PLUG/pages/15826959/Source+code+license+agreement)

## Manual
Please refer to the Wiki pages of this repository.

## Branches
The sources contain two branches. As the "dev" branch is work in progress you should only use the sources of the master branch.

## Building

### Building for development
When you want to run the application in "development" mode you have to

1. Comment out "spring.profiles.active: production" in application.yml
2. Adopt the baseUrl in application.yml to the URL your add-on is reachable 
2. Run the following command

mvn spring-boot:run -P dev
 

### Building for Heroku
The sources are ready to be deployed to Heroku as they are. Of course you have to adopt the baseUrl in atlassian-connect.json to the URL of your dyno.

## Change log

### 1.15.10

Infrastructure:

* Upgrade to Spring Boot 3.4.3
* Upgrade to ACSB 5.1.7


### 1.15.9

Infrastructure:

* Upgrade to Spring Boot 3.4.1


### 1.15.9

Infrastructure:

* Upgrade ACSB


### 1.15.7

Infrastructure:

* Upgrade to Spring Boot 3.2.12


### 1.15.6

Infrastructure:

* Upgrade to Spring Boot 3.2.11
* Upgrade to ACSB 5.1.4


### 1.15.5

Infrastructure:

* Upgrade to Spring Boot 3.2.5

### 1.15.4

Infrastructure:

* Upgrade to Spring Boot 3.2.3
* Upgrade to ACSB 4.1.1
* Upgrade other dependencies

### 1.15.3

Improvements:

* Show error message in case pages for migration can't be collected

### 1.15.2

Bug fixes:

* Allow migration of cite Ids with special characters

### 1.15.1

Improvements:

* Allow migration of data which contains AttachmentResourceIdentifier

### 1.15.0

New features:

* Add some basic text styles to the former plain cites
* Allow to define other locations not just pages

Infrastructure:

* Upgrade to Spring Boot 3.1.7

Bug fixes:

* Fix wrong content detection during migration in case a page was edited

### 1.14.0

New features:

* Allow selection of existing cites in short cite

### 1.13.9

Infrastructure:

* Upgrade to Spring Boot 3.1.6
* Upgrade to ACSB 4.0.7

### 1.13.8

Infrastructure:

* Upgrade to Spring Boot 3.1.5
* Upgrade to ACSB 4.0.6
* Switch to Confluence REST API v2

### 1.13.7

Infrastructure:

* Upgrade to json 20231013

### 1.13.6

Infrastructure:

* Upgrade to Spring Boot 3.1.4
* Upgrade to ACSB 4.0.3

### 1.13.5

Infrastructure:

* Upgrade to Spring Boot 2.7.16
* Upgrade to ACSB 3.0.7

### 1.13.4

Improvements:

* Re-enable theming with proper "no theme" detection

### 1.13.3

Bug fixes:

* Disable theming again due to UI issues on some systems

### 1.13.2

Bug fixes:

* Fix error related to some DOI formats

### 1.13.1

Infrastructure:

* Upgrade to Spring Boot 2.7.14
* Override Thymeleaf version to 3.1.2

### 1.13.0

New features:

* Compatibility with dark mode

### 1.12.3

Infrastructure:

* Upgrade to Spring Boot 2.7.13
* Upgrade ACSB to 3.0.4

### 1.12.2

Infrastructure:

* Upgrade to Spring Boot 2.7.11
* Upgrade other dependencies

### 1.12.1

Improvements:

* Only use compiled reference in case length is bigger than 3 to avoid use errors

### 1.12.0

New features:

* Add direct BibTeX entry macro
* Allow bibliography output
* Switch to select boxed when importing cites and using BibTeX file cites

Infrastructure:

* Upgrade to Spring Boot 2.7.10
* Switch to managed task executors
* Use Java 17 as base
* Remove some CDNs

### 1.11.0

New features:

* BibTeX listing macro

Infrastructure:

* Upgrade to Spring Boot 2.7.9
* Upgrade to Atlassian Connect Spring Boot 3.0.2

Bug fixes:

* Fix detection of BibTeX files during migration

### 1.10.13

Bug fixes:

* Set Content-Disposition to attachment when downloading external BibTeX files

Infrastructure:

* Upgrade to Spring Boot 2.7.7

### 1.10.12

Infrastructure:

* Upgrade to Spring Boot 2.7.6
* Upgrade postgresql

### 1.10.11

Infrastructure:

* Check for https at the beginning for leading external bib files

### 1.10.10

Infrastructure:

* Upgrade to Spring Boot 2.7.4
* Only allow https connections when reading external web content

### 1.10.9

Infrastructure:

* Upgrade to Spring Boot 2.7.3

### 1.10.8

Infrastructure:

* Upgrade to Spring Boot 2.7.2
* Upgrade to postgresql to 42.4.1

### 1.10.7

Bug fixes:

* Fix link type selection issue

### 1.10.6

Infrastructure:

* Remove DOI proxy for older version of the Server/DC app
* Remove all inline scripts
* Set Content-Security-Policy
* Upgrade to Spring Boot 2.7.1
* Upgrade to Atlassian Connect Spring Boot 2.3.6
* Change to early escaping of user inputs

Bug fixes:

* Correct documentation links

### 1.10.5

Bug fixes:

* Respect JWT on migration endpoint

### 1.10.4

Infrastructure:

* Upgrade to Spring Boot 2.7.0
* Upgrade to Atlassian Connect Spring Boot 2.3.5

Bug fixes:

* Check that the url used for downloading external BibTeX files does not point to this app


### 1.10.3

Infrastructure:

* Upgrade to Spring Boot 2.6.8


### 1.10.2

Infrastructure:

* Upgrade to Atlassian Connect Spring Boot 2.3.4


### 1.10.1

New features:

* Roll-out of the migration support


### 1.10.0

New features:

* Prepare better migration support (functional roll-out two days later due to Marketplace constraints)
* Allow to show CSL references in case the style provides them


### 1.9.4

Infrastructure:

* Upgrade to Atlassian Connect Spring Boot 2.3.3
* Upgrade to Spring Boot 2.6.6


### 1.9.3

Infrastructure:

* Upgrade to Atlassian Connect Spring Boot 2.3.2
* Upgrade to Spring Boot 2.6.5


### 1.9.2

Infrastructure:

* Upgrade to Atlassian Connect Spring Boot 2.3.1
* Upgrade to Spring Boot 2.6.4
* Upgrade other dependencies


### 1.9.1

Infrastructure:

* Upgrade to Spring Boot 2.5.10


### 1.9.0

Bug fixes:

* Fix link to attachments issue (only for newly inserted cites)

Infrastructure:

* Upgrade citation.js to 0.5.4


### 1.8.7

Infrastructure:

* Upgrade to Spring Boot 2.5.9 with overriding the Tomcat version


### 1.8.6

Infrastructure:

* Upgrade postgresql driver to avoid CVE-2022-21724


### 1.8.5

Infrastructure:

* Go back to Spring Boot 2.5.6 to avoid https://bugs.launchpad.net/ubuntu/+source/linux/+bug/1924298


### 1.8.3 and 1.8.4

Infrastructure:

* Update to Spring Boot 2.5.8
* Update to Atlassian Connect Spring Boot 2.2.6


### 1.8.2

Improvements:

* Update bundled libs


### 1.8.1

Improvements:

* Improve the messages shown


### 1.8.0

Improvements:

* Process cites at the client not in phantom any more
* Show error details in case rendering of BibTeX or DOI fails
* Upgrade to Atlassian Connect Spring Boot 2.2.3


### 1.7.2

Bug fixes:

* Context QSH related fix as required by Atlassian


### 1.7.1

Bug fixes:

* Context QSH related fix as required by Atlassian


### 1.7.0

New features:

* Allow to specify an URL to a BibTeX file in addition to attachments
* Add a proxy to our phantom service for Simple Cite Server and DC

Bug fixes:

* Fix doi proxy issues


### 1.6.0

New features:

* Allow to link to links embedded in a BibTeX entry 


### 1.5.1

Improvements:

* Resolve coverity defects (non-critical)


### 1.5.0

Improvements:

* Add a hidden macro single-cite-short for migration purposes


### 1.4.7

Improvements:

* Increase time out of DOI render requests to 9900 ms


### 1.4.6

Improvements:

* Disable numbered list output in the summary in case more than 100 cites need to be shown


### 1.4.5

Improvements:

* Further improve request limiting
* Update Spring Boot and Atlassian Connect Spring Boot


### 1.4.4

Improvements:

* Avoid to many concurrent requests


### 1.4.1 to 1.4.3

Improvements:

* Avoid the fallback to the app user


### 1.4.0

Improvements:

* Add the single cite plain text macro to overcome the inline rendering issue


### 1.3.21

Infrastructure:

* Add additional endpoint for server versions of Simple Cite


### 1.3.20

Improvements:

* Limit the number of requests send to the Confluence instance


### 1.3.19

Infrastructure:

* Upgrade to Atlassian Connect Sprint Boot 2.0.1
* Upgrade to Spring Boot Version 2.1.7
* Take all.js from connect-cdn.atl-paas.net


### 1.3.18

Improvements:

* Do not show reference to define only cite


### 1.3.16 and 1.3.17

Bug fixes:

* Allow requests as host user again


### 1.3.15

Improvements:

* Add read timeout for DOI requests


### 1.3.14

Improvements:

* Improve logging


### 1.3.13

Improvements:

* Only wait a certain time for the Confluence page request


### 1.3.12

Infrastructure:

* Send GDPR true
* Upgrade to Atlassian Connect Sprint Boot 1.5.1


### 1.3.11

Improvements:

* Allow nested macros in cites


### 1.3.10

Improvements:

* Change cache control


### 1.3.9

Bug fixes:

* Allow cite summary for anonymous users


### 1.3.8

Improvements:

* Catch cite summary exceptions
* Do not remove the cache entry when empty


### 1.3.7

Improvements:

* Allow empty cite map in cache

Infrastructure:

* Upgrade to Atlassian Connect Spring Boot version 1.4.3

### 1.3.6

Bug fixes:

* Fix macro body detection

### 1.3.5

Others:

* Enforce valid license

### 1.3.4

Bug fixes:

* Allow links to attachments on restricted pages

### 1.3.3

Infrastructure:

* Load content as user and if not successful as add-on

### 1.3.2

Infrastructure:

* Add license reminder

### 1.3.1

Infrastructure:

* Adopt to changes in the phantom based BibTeX service

### 1.3.0

Improvements:

* Add "BibTeX cite" macro which allows rendering BibTeX entries

### 1.2.1

Bug fixes:

* Also search for DOI cites in the space cite summary search

### 1.2.0

Improvements:

* Add "DOI cite" macro which allows referring DOIs

### 1.1.2

Infrastructure:

* Add DOI proxy for the server version

### 1.1.1

Improvements:

* Enable caching headers again

### 1.1.0

Improvements:

* Add "Single cite import" macro which allows referring to cites defined on other pages

### 1.0.3

Infrastructure:

* Update to Atlassian Connect Spring Boot version 1.3.5
* Update to Spring Boot version 1.5.3

### 1.0.2

Bug fixes:

* Search as user for the Space Cite Summary currently seems to be broken; perform search as add-on

### 1.0.1

Bug fixes:

* Remove wrong @ResponseBody tags

### 1.0.0

initial release


