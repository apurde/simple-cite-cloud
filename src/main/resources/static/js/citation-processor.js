// process a BibTeX or DOI cite
function processBibTeXOrDOI(citeID, rawData, style, type, success, error) {
	getTemplate(style).then(function(template) {
		var citeData = processCite(citeID, rawData, style, type, template);
		success(citeData);
	})
	.catch(function(err) {
		console.log("Error processing cite: " + err);
		err = err.length > 100 ? err.substring(0, 100) + "..." : err; 
		error(err);
	});	
};


// get a CSL template
function getTemplate(template) {
	return new Promise(function (resolve, reject) {
		var request = new XMLHttpRequest();
		request.open("GET", "https://raw.githubusercontent.com/citation-style-language/styles/master/" + template + ".csl");
		request.timeout = 2000;
		request.onload = function() {
			if (this.status >= 200 && this.status < 300) {
				resolve(request.response);
			}
			else {
				reject("Could not load citation template");
			}
		};
		request.onerror = function () {
			reject("Could not load citation template");
		};
		request.send();
	});
};


// process the cite with citation.js
function processCite(citeID, rawData, style, type, template) {

	var Cite = require('citation-js');

	if((style != "apa") && (style != "vancouver") && (style != "harvard1")) {
		Cite.CSL.register.addTemplate(style, template);
	}

	var cite;
	if(type === "doi") {
		cite = new Cite("https://doi.org/" + rawData, { forceType : "@doi/api"});	
	}
	else {
		cite = new Cite(rawData);	
	}

	var citeBib = cite.format('bibliography', {
	  format: 'text',
	  template: style,
	  lang: 'en-US'
	});
	citeBib = citeBib.replace(/^1\.|\(1\)|\[1\]/, "").replace(/\r?\n|\r/g, "");
	
	var citeRef = cite.format('citation', {
	  format: 'text',
	  template: style,
	  lang: 'en-US'
	});
	if(/^1\.|\(1\)|\[1\]/.test(citeRef)) {
		citeRef = "";
	}
	
	var citeData = cite.format('data', {
	  format: 'object',
	  template: style,
	  lang: 'en-US'
	});
	
	return {
		cite : citeBib,
		reference : citeRef,
		link : citeData[0].URL || "",
		citeID : citeID
	};
};