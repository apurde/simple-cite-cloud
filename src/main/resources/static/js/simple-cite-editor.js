/*jshint esversion: 11 */

function CiteEditor() {
	
	var baseUrl = "";
	var caller = "";
	var compiledCite = "undefined";
	var compiledReference = "undefined";
	var addedLink = "";
	var currentPageResult = [];
	var currentSearchTerm = "";
	var errorMessage = "";
	var citeProcessor;
	var macroBody = "";
	var uuid = "";
	

	this.init = function(processor) {
		baseUrl = $("#messagesMore").data("base-url");
		caller = $("#messagesMore").data("caller");
		citeProcessor = processor;
		uuid = self.crypto.randomUUID();
		
		AP.confluence.getMacroData(function(macroParams) {
			initGui(macroParams, $("#importFromPlaceholder").text(), $("#attPlaceholder").text(), $("#linkPlaceholder").text(), $("#confluenceContentPlaceholder").text(), baseUrl, caller);
		});	
				
		AP.confluence.getMacroBody(function(body){
  			macroBody = body;
  			if(caller === "single-cite-plain") {
				AP.confluence.getMacroData(function(macroParams) {
	  				$("#citeText").val((macroParams.compiledCite && macroParams.compiledCite !== "undefined") ? macroParams.compiledCite : macroBody);
	  				setTimeout(function() {
						initTiny();
					}, 150); // wait until theming is set
				});
  			}
  			if(caller === "dbibtex-cite") {
  				$("#dBibTeX").val(macroBody);
  			}
		});
		
		AP.dialog.disableCloseOnSubmit();
		AP.events.on('dialog.button.click', function(data){
  			if(data.button.name === 'submit') {
    			close1();
  			}
		});
		
		$("#addLinkType").change(function() {
			linkTypeChange();
		});
	};	
	
	
	var initTiny = function() {
		tinymce.init({
			selector: '#citeText',
			toolbar: 'bold italic underline | subscript superscript | forecolor backcolor | link',
			plugins: 'link',
			menubar: false,
			statusbar: false,
			link_target_list: false,
			height: "170px",
			content_css : $("html").attr("data-color-mode") === "dark" ? ["dark", "css/tiny-cite-editor-dark.css", "css/tiny-cite-editor.css"] : ["css/tiny-cite-editor.css"],
			skin: $("html").attr("data-color-mode") === "dark" ? "oxide-dark" : undefined,
		});
	};
	
	
	var close1 = function () {
		if(caller === "doi-cite" || caller === "bibtex-cite" || caller === "bibtex-listing" || caller === "dbibtex-cite") {
		
			// disable all elements
			AP.dialog.getButton("submit").disable();
			$('#paramFields *').not("option").attr('disabled', true);
			
			// hide last error
			$("#errorGroup").hide();
			
			// indicate that we are doing something
			$("#spinnerGroup").show();
			
			if(caller === "doi-cite") {
				$.when(
					compileDOICite(baseUrl)
				).done(function() {		
					return close2();
				});	
			}
			else {
				$.when(
					compileBibTeXCiteOrListing(baseUrl, caller)
				).done(function() {		
					return close2();
				});	
			}
		}
		else if(caller == "single-cite-plain") {
			compiledCite = tinymce.EditorManager.activeEditor.getContent();
			errorMessage = compiledCite.length > 500 ? $("#plainCiteTooLong").text() : ""; 
			macroBody = "";
			compilePlainCite(baseUrl);
			return close2();
		}
		else {
			return close2();
		}
	};
	
	
    var close2 = function() {	
   		if(errorMessage.length === 0) {
    		// get the current parameters
			var macroParams = guiToMacroParams();
			
			// save macro parameters
			AP.confluence.saveMacro(macroParams, macroBody);
			
			// close the editor
			AP.confluence.closeMacroEditor();
			
			return true;
   		}
   		else {
   			// stop the spinner
   			$("#spinnerGroup").hide();
   			
   			// enable fields again
   			$('#paramFields *').attr('disabled', false);
   			AP.dialog.getButton("submit").enable();
   			
   			// show the error message
   			$("#errorDetails").text(errorMessage);
   			$("#errorGroup").show();
   			
   			return false;
   		}
	};
	
	
	// initialize GUI
	var initGui = function(macroParams, importFromPlaceholder, attPlaceholder, linkPlaceholder, confluenceContentPlaceholder, pbaseUrl, caller) {
		
		// enable UI elements based on caller
		if(caller == "single-cite-import") {
			$("#importFromGroup").show();
		}
		if(caller === "doi-cite") {
			$("#styleGroup,#addLinkTypeGroup,#cslStyleReferenceGroup,#bibOutputGroup").show();
		}
		if(caller === "bibtex-cite") {
			$("#bibTeXFileGroup,#addLinkTypeGroup,#styleGroup,#cslStyleReferenceGroup,#bibOutputGroup,#bibOutputGroup").show();
		}
		if(caller === "dbibtex-cite") {
			$("#dBibTeXGroup,#addLinkTypeGroup,#styleGroup,#cslStyleReferenceGroup,#bibOutputGroup").show();
		}
		if(caller === "single-cite-plain") {
			$("#plainGroup,#addLinkTypeGroup,#bibOutputGroup").show();
		}
		if(caller === "bibtex-listing") {
			$("#bibTeXFileGroup,#styleGroup").show();
			$("#idGroup,#pageNumbersGroup,#optionsGroup").hide();
		}
		
		// store values for later user
		baseUrl = pbaseUrl;
		
		// the simple stuff
		$('#citeID').val(macroParams && macroParams.citeID || "");
		$('#pageNumbers').val(macroParams && macroParams.pageNumbers || "");
		$('#superscript').prop("checked", (( macroParams && macroParams.superscript || "true") == "true"));
		$('#cslReference').prop("checked", (( macroParams && macroParams.cslReference || "false") == "true"));
		$('#bibOutput').prop("checked", (( macroParams && macroParams.bibOutput || "false") == "true"));
		$('#style').val(macroParams && macroParams.style || "");
		$('#addLinkType').prop('selectedIndex', macroParams &&  macroParams.addLinkType || 0);
		var oldAddLinkFlag = ((macroParams && macroParams.addLink || "false") == "true");
		if(oldAddLinkFlag) {
			$('#addLinkType').prop('selectedIndex', 3);
		}
		$('#addLinkExternal').val(macroParams && macroParams.addLinkExternal || "");
				
		// init "import from" selection
		var importFrom = macroParams && macroParams.importFrom || "";
		$('#importFrom').val(importFrom);
		confluenceContentLoad("#importFrom", "page", importFrom, importFromPlaceholder);
		
		// init "bib-file attachment selection"
		var bibTeXFileID = macroParams && macroParams.bibTeXFileID || "";
		$('#attachment').val(bibTeXFileID);
		if(bibTeXFileID.length === 0 || bibTeXFileID.startsWith("att")) {
			confluenceAttachmentLoad(bibTeXFileID, attPlaceholder);
		}
		else {
			$("#attachment").attr("placeholder", linkPlaceholder);
		}
		confluenceAttachmentLoadLocal();
		
		// init "add link Confluence" selection
		var addLinkConfluence = macroParams && macroParams.addLinkConfluence || "";
		$('#addLinkConfluence').val(addLinkConfluence);
		confluenceContentLoad("#addLinkConfluence", "page,attachment", addLinkConfluence, confluenceContentPlaceholder);
		linkTypeChange();
		
		// store the compiled cite if present
		compiledCite = macroParams && macroParams.compiledCite || compiledCite;
		compiledReference = macroParams && macroParams.compiledReference || compiledReference;
		
		// react on the toggle button between attachment and url
		$("#switch").click(function() {
			if($("#attachment").data("select2")) {
				$("#attachment").attr("placeholder", linkPlaceholder);
				$("#attachment").auiSelect2("destroy");
			}
			else {
				$("#attachment").attr("placeholder", attPlaceholder);
				confluenceAttachmentLoad(bibTeXFileID, attPlaceholder);
			}
		});
		
		// insert iframe for citation processing
		var iframe = document.createElement('iframe');
		iframe.style.display = "none";
		iframe.id="cite-processor";
		iframe.src = "/citation-processor";
		document.body.appendChild(iframe);
		
		// allow the selection of citeIds in case of imported cites
		if(caller === "single-cite-import") {
			if($('#importFrom').val()) {
				setCiteIdOptions($('#importFrom').val(), macroParams?.citeID || "");
			}
			$("#importFrom").change(function() {
				setCiteIdOptions($('#importFrom').val(), "");		
			});
		}
		
		// allow the selection of citeIds in case of bibtex file cites
		if(caller === "bibtex-cite") {
			if(isValidHttpsUrl($("#attachment").val())) {
				setCiteIdOptions($('#attachment').val(), macroParams?.citeID || "");
			}
			$("#attachment").on("attachment-init-done", function() {
				setCiteIdOptions($('#attachment').val(), macroParams?.citeID || "");
			});
			$("#attachment").on("change", function() {
				if($('#attachment').val().startsWith("att") || isValidHttpsUrl($("#attachment").val())) {
					setCiteIdOptions($('#attachment').val(), "");
				}
			});
		}
		
		// set citeID when empty in case of direct BibTeX entry
		$("#dBibTeX").on("focusout", function() {
			if($("#citeID").val() === "") {
				var idRegEx = /(@[a-zA-Z ]*)(\{)(.*?)(,)/g;
				var id;
				id = idRegEx.exec($("#dBibTeX").val());
				if(id !== null) {
					$("#citeID").val(id[3]);
				}
			}
		});
		
		
		// init single cite short
		if(caller === "short-cite") {
			initSelectWithLOcalCites(macroParams?.citeID || "");
		}
		
		// react on the reload button for single-cite-short
		$("#reload").click(function() {
			initSelectWithLOcalCites($('#citeID').val());
		});
	};
	
	
	// store the values from the dialog GUI into an object
	var guiToMacroParams = function() {
		
		var macroParams = {
			citeID: $('#citeID').val(),
			pageNumbers: $('#pageNumbers').val(),
			superscript: $('#superscript').prop("checked") ? "true" : "false",
			cslReference : $('#cslReference').prop("checked") ? "true" : "false",
			bibOutput : $('#bibOutput').prop("checked") ? "true" : "false",
			importFrom: $('#importFrom').val(),
			bibTeXFileID : $('#attachment').val(),
			style: $('#style').val(),
			addLinkType : $('#addLinkType').prop('selectedIndex'),
			addLinkConfluence : $('#addLinkConfluence').val(),
			addLinkExternal : $('#addLinkExternal').val(),
			compiledCite : compiledCite,
			compiledReference : compiledReference,
			addedLink : addedLink,
			uuid : uuid
		};
		
		return macroParams;
	};
	
	
	// Confluence content load
	var confluenceContentLoad = function(element, allowedTypes, initialContentID, placeholderText) {
	 		
		$(element).auiSelect2({
		
			// let the user enter three letters before we search
		    minimumInputLength: 3,
		    
		    // set the placeholder
		    placeholder: placeholderText,
		    
		    // let select2 load matching titles using CQL in a rest call
		    ajax: {
		        url: '/rest/api/content/search',
		        dataType: 'json',
		        type: "GET",
		        data: function (term) {
		            return {
		                cql: "type IN (" + allowedTypes + ") AND title~'"+ term + "*'",
		                expand: "space"
		            };
		        },
		        // map the results to a select2 format
		        results: function (responseJSON) {
		        	var response = JSON.parse(responseJSON);
		            return {
		                results: $.map(response.results, function (item) {
		                    return {
		                        text: item.title + " (" + item.space.key + ")",
		                        id: item.id,
		                        webui: item._links.webui
		                    };
		                })
		            };
		        },
		        // we need a modified transport layer to handle proper signing of the request
		        transport: function transport(params) {
	                AP.request({
	                    url: params.url,
	                    headers: {
	                        "Accept": "application/json"
	                    },
	                    data: params.data,
	                    success: params.success,
	                    error: params.error
	                });
	    		}	
		    },
		    
		    // the initial value
		    initSelection: function (element, callback) {
	    		if(initialContentID.length > 0) {
					apiV2Helper.getContentAndSpaceFromId({
						contentId: initialContentID,
						success: function(response) {
							var confContent = {
								"id": response.id,
								"text": response.title + " (" + response.space.key + ")",
								"webui": response._links.webui
							};
							callback(confContent);
						},
						error: function (response) {
							console.log("Error occured in : " + response);
						}
					});


				}
		    }
		});
	};
		
	
	// Confluence attachment load
	var confluenceAttachmentLoad = function(attachmentID, attPlaceholder) {
	 		
		$("#attachment").auiSelect2({
		
			// let the user enter three letters before we search
		    minimumInputLength: 3,
		    
		    // set the placeholder
		    placeholder: attPlaceholder,
		    
		    // let select2 load matching titles using CQL in a rest call
		    ajax: {
		        url: '/rest/api/content/search',
		        dataType: 'json',
		        type: "GET",
		        data: function (term) {
		        	currentSearchTerm = term;
		            return {
		                cql: "(title~'"+ term + "*') AND (type=attachment)",
		                expand: "container",
		            };
		        },
		        // map the results to a select2 format
		        results: function (responseJSON) {
		        	var response = JSON.parse(responseJSON);
		        	var filteredResult = [];
					for(var n = 0; n < response.results.length; n++) {
						if(response.results[n].title.toLowerCase().indexOf('.bib') > -1) {
							var element = {
								id: response.results[n].id,
								text: response.results[n].title + " (" + response.results[n].container.title + ")",
								download: response.results[n]._links.download
								};
							filteredResult.push(element);
						}
					}
					var filteredResultJSON = JSON.stringify(filteredResult);
					for(n = 0; n < currentPageResult.length; n++) {
						if((filteredResultJSON.indexOf(currentPageResult[n].id) == -1) &&
							(currentPageResult[n].text.toLowerCase().indexOf(currentSearchTerm.toLowerCase()) > -1))  {
							filteredResult.push(currentPageResult[n]);
						}
					}
					return {results : filteredResult };
		        },
		        // we need a modified transport layer to handle proper signing of the request
		        transport: function transport(params) {
	                AP.request({
	                    url: params.url,
	                    headers: {
	                        "Accept": "application/json"
	                    },
	                    data: params.data,
	                    success: params.success,
	                    error: params.error
	                });
	    		}	
		    },
		    
		    // the initial value in the box
		    initSelection: function (element, callback) {
	    		if(attachmentID.length > 0) {
					apiV2Helper.getAttachmentAndTitleFromId({
						contentId: attachmentID,
						success: function (response) {
							var confContent = {
								"id": response.id,
								"text": response.title + " (" + response.containertitle + ")",
								"download": response._links.download
							};
							callback(confContent);
							$("#attachment").trigger("attachment-init-done");
						},
						error: function (response) {
							console.log("Error occured in : " + response);
						}
					});
				}
			}
		}).auiSelect2('val', []);
	};
	
	
	// Confluence attachment load local page
	var confluenceAttachmentLoadLocal = function() {
		AP.navigator.getLocation(function (location) {
			var currentPageId = location.context.contentId;
			apiV2Helper.getAttachmentsforPageId({
				pageId: currentPageId,
				success: function(response) {
			  
				  for (var n = 0; n < response.results.length; n++) {
					if (response.results[n].title.toLowerCase().indexOf('.bib') > -1) {
					  var element = {
						id: response.results[n].id,
						text: response.results[n].title,
						download: response.results[n]._links.download
					  };
					  currentPageResult.push(element);
					}
				  }
				},
				error: function(response) {
				  console.log("Error occured in : " + response);
				}
			});
		});
	};
	
	
	// change available fields based on selected link type
	var linkTypeChange = function() {
		var linkType = $('#addLinkType').prop('selectedIndex');
		switch(linkType) {
			case 1:
				$("#addLinkExternalGroup").hide();
				$("#addLinkConfluenceGroup").show();
				break;
			case 2:
				$("#addLinkExternalGroup").show();
				$("#addLinkConfluenceGroup").hide();
				break;
			default:
				$("#addLinkExternalGroup").hide();
				$("#addLinkConfluenceGroup").hide();
				break;
		}
	};
	
	
	// compile a DOI cite
	var compileDOICite = function(baseUrl) {
		var dfd = $.Deferred();
		errorMessage = "";
		var macroParams = guiToMacroParams();
		
		citeProcessor.compileDOICite(macroParams.citeID, (macroParams.style == "") ? "ieee" : macroParams.style).then((result) => {
			compiledCite = result.cite;
			compiledReference = result.reference;
			errorMessage = result.error || "";
			addedLink = compileLink(baseUrl, "");
			return dfd.resolve();
		});
		
		return dfd.promise();
	};
	
	
	// compile a BibTeX cite
	var compileBibTeXCiteOrListing = function(baseUrl, caller) {
		var dfd = $.Deferred();
		errorMessage = "";
		var macroParams = guiToMacroParams();
		var selectData = $('#attachment').auiSelect2("data");
		var isAttachment = $('#attachment').val().startsWith("att");
		var url = isAttachment ? selectData && selectData.download || "undefined" : $('#attachment').val();
		
		if(caller === "bibtex-listing") {
			citeProcessor.compileBibTeXListing((macroParams.style == "") ? "ieee" : macroParams.style, url, isAttachment).then((result) =>  {
				compiledCite = JSON.stringify(result.cites);
				compiledReference = "";
				errorMessage = result.error || "";
				addedLink = "";
				putToPreviewCache(uuid, compiledCite);
				return dfd.resolve();
			});
		}
		else if (caller === "dbibtex-cite") {
			macroBody = $('#dBibTeX').val();
			citeProcessor.compileDirectBibTeXCite((macroParams.style == "") ? "ieee" : macroParams.style, macroBody).then((result) =>  {
				compiledCite = result.cite;
				compiledReference = result.reference || "";
				errorMessage = result.error || "";
				addedLink = compileLink(baseUrl, result.link || "");
				return dfd.resolve();
			});
		}
		else {		
			citeProcessor.compileBibTeXCite(macroParams.citeID,  (macroParams.style == "") ? "ieee" : macroParams.style, url, isAttachment).then((result) =>  {
				compiledCite = result.cite;
				compiledReference = result.reference || "";
				errorMessage = result.error || "";
				addedLink = compileLink(baseUrl, result.link || "");
				return dfd.resolve();
			});
		}
		
		return dfd.promise();
	};
	
	
	// compile the plain cite
	var compilePlainCite = function(baseUrl) {
		addedLink = compileLink(baseUrl, "");
	};
	
	
	// compile link
	var compileLink = function(baseUrl, bibTeXLink) {
		var linkType = $('#addLinkType').prop('selectedIndex');
		switch(linkType) {
			case 1:
				var id = $('#addLinkConfluence').val();
				var content =  $('#addLinkConfluence').select2("data");
				if(id.indexOf("att") > -1) {
					return baseUrl + content.webui;
				}
				else {
					return baseUrl + "/pages/viewpage.action?pageId=" + content.id;
				}
			case 2:
				return $('#addLinkExternal').val();
			case 3:
				return "http://dx.doi.org/" + $('#citeID').val();
			case 4:
				return bibTeXLink;
			default:
				return "";
		}
	};
	
	
	// put to preview cache
	var putToPreviewCache = function(uuid, preview) {
		AP.context.getToken(function(token){
			$.ajax({
				url: "/cache?uuid=" + uuid,
				type: "POST",
				data: preview,
				contentType : "application/json",
				dataType : "json",
				beforeSend: function (request) {
				    request.setRequestHeader("Authorization", "JWT " + token);
				}
			});
		});
	};
	
	
	// set all available citeIDs as option
	var setCiteIdOptions = async function(source, initWithValue) {
		$("#citeID").empty();
		$("#citeID").append('<option disabled selected hidden>' + $("#gettingContent").text() + '</option>');		
		if(source && source.length > 0) {
			var isAttachment = source.startsWith("att");
			var isUrl = source.startsWith("https://"); 
			var citeIds = [];
			if(isAttachment || isUrl) {
				var url = isAttachment ? $('#attachment').auiSelect2("data").download : source;
				var result = await citeProcessor.getBibTeXEntryNames(url, isAttachment);
				citeIds = result.entryNames;
			}
			else {
				citeIds = await getCiteIdsOnPage(source);
			}
			$("#citeID").empty();
			$("#citeID").append('<option disabled selected hidden>' + $("#pleaseSelect").text() + '</option>');
			for(var n = 0; n < citeIds.length; n++) {
				$("#citeID").append('<option>' + sanitizeText(citeIds[n]) + '</option>');
			}
			if(initWithValue.length > 0) $("#citeID").val(initWithValue);
		}
	};
	
	
	// get all citeIds from the given page
	var getCiteIdsOnPage = async function(pageId) {
		var citeIds = [];
		var pageRequest = await AP.request("/api/v2/pages/" + pageId + "?body-format=storage&get-draft=true");
		var pageRequestBody =  JSON.parse(pageRequest.body);
		var body = pageRequestBody.body.storage.value;
		
		body = body.replace(/<ac:structured-macro ac:name="single-cite-import".*?<\/ac:structured-macro>/gs, "");
		
		var citeIdRegEx = /(<ac:parameter ac:name="citeID">)(.*?)(<\/ac:parameter>)/gs;
		var citeId;
		while((citeId = citeIdRegEx.exec(body)) !== null) {
			if(!citeIds.includes(citeId[2])) citeIds.push(citeId[2]);
		}
				
		return citeIds;
	};
	
	
	var sanitizeText = function(text) {
		return $('<div/>').text(text).html();
	};
	
	
	// check if the provided string is a valid https url
	var isValidHttpsUrl = function(string) {
		let givenURL;
		try {
			givenURL = new URL(string);
		}
		catch (error) {
			return false;  
		}
		return givenURL.protocol === "https:";
	};
	
	
	// init the citeID selection with local cite
	var initSelectWithLOcalCites = function(citeID) {
		AP.navigator.getLocation(function(location) {
			setCiteIdOptions(location.context.contentId, citeID);
		});
	};
}

var editor = new CiteEditor();
var processor = new CiteProcessor();
editor.init(processor);


