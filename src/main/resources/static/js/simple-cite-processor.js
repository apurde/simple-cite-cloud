function CiteProcessor() {
	
	this.compileDOICite = function(citeID, style)  {
		return compileDOICiteI(citeID, style);
	};
	
	
	this.compileBibTeXCite = function(citeID, style, url, isAttachment)  {
		return compileBibTeXCiteI(citeID, url, isAttachment, style);
	};
	
	
	this.compileDirectBibTeXCite = function(style, entry)  {
		return compileDirectBibTeXCiteI(entry, style);
	};
	
	
	this.compileBibTeXListing = function(style, url, isAttachment) {
		return compileBibTeXListingI(url, isAttachment, style);
	};
	
	
	this.getBibTeXEntryNames = function(url, isAttachment) {
		return getBibTeXEntryNamesI(url, isAttachment);
	};
	
	
	// compile a DOI cite
	var compileDOICiteI = function(doi, style) {
		
		var dfd = $.Deferred();
		
		// use the citation processor
		document.getElementById("cite-processor").contentWindow.processBibTeXOrDOI(doi, doi, style, "doi", function(citeData) {
			return dfd.resolve({
				cite : citeData.cite,
				reference: citeData.reference,
				link: "http://dx.doi.org/" + doi
			});
		}, function(err) {
			return dfd.resolve({
				cite : "undefined",
				reference : "undefined",
				error : err
			});
		});
		
		return dfd.promise();
	};
	
	
	// compile a BibTeX cite
	var compileBibTeXCiteI = function(citeID, url, isAttachment, style) {
		
		var dfd = $.Deferred();
		
		loadBibFile(url, isAttachment,
			function(result) {
				extractBibTeXEntryAndProcess(citeID, style, dfd, result);	
			},
			function() {
				return dfd.resolve({
					cite : "undefined",
					reference : "undefined",
					error : "could not read bib-file"				
				});
			}
		);
		
		return dfd.promise();
	};
	
	
	// compile a directly entered BibTeX cite
	var compileDirectBibTeXCiteI = function(entry, style) {
		var dfd = $.Deferred();
		extractBibTeXEntryAndProcess("@direct", style, dfd, entry);
		return dfd.promise();
	};
	
	
	// compile a BibTeX listing
	var compileBibTeXListingI = function(url, isAttachment, style) {
		
		var dfd = $.Deferred();
		
		loadBibFile(url, isAttachment,
			function(result) {
				extractBibTeXEntriesAndProcess(style, dfd, result);	
			},
			function() {
				return dfd.resolve({
					cites : [],
					error : "could not read bib-file"				
				});
			}
		);
		
		return dfd.promise();
	};
	
	
	// get all BibTeX entry names
	var getBibTeXEntryNamesI = function(url, isAttachment) {
		var dfd = $.Deferred();
		
		loadBibFile(url, isAttachment,
			function(result) {
				return dfd.resolve({
					entryNames: extractAllBibTeXEntryNames(result)
				});	
			},
			function() {
				return dfd.resolve({
					entryNames : [],
					error : "could not read bib-file"				
				});
			}
		);
		
		return dfd.promise();
	};
	
	
	// load bin file
	var loadBibFile = function(url, isAttachment, success, error) {
		
		// get Confluence attachment BibTeX file 
		if(isAttachment) {
			AP.request({
				url : url,
				type: 'GET',
				contentType: 'text/plain',
				success: function(result) {
					success(result);
				},
				error: function() {
					error();
				}
			});
		}
		
		// get external BibTeX file
		else {
			AP.context.getToken(function(token){
				$.ajax({
					url: "/external?url=" + url,
					beforeSend: function (request) {
					    request.setRequestHeader("Authorization", "JWT " + token);
					},
					success: function(result) {
						success(result);
					},
					error: function() {
						error();
					}
				});
			});
		}
	};
	
	
	// extract a single BibTeX entry and process it
	var extractBibTeXEntryAndProcess = function(citeID, style, dfd, bibTeXFileContent) {
		
		// relevant data
		var bibTeXEntry = citeID === "@direct" ? bibTeXFileContent : extractBibTeXEntry(bibTeXFileContent, citeID);
	
		// no bibTeX entry found
		if(bibTeXEntry.length == 0) {
			return dfd.resolve({
				compiledCite : "undefined",
				error : "the BibTeX entry could not be extracted"		
			});
		}
		
		// use the citation processor
		document.getElementById("cite-processor").contentWindow.processBibTeXOrDOI(citeID, bibTeXEntry,  style, "bibtex", function(citeData) {
			return dfd.resolve({
				cite : citeData.cite,
				link : extractURLFromBibTeXEntry(bibTeXEntry),
				reference : citeData.reference
			});
		}, function(err) {
			return dfd.resolve({
				cite : "undefined",
				reference : "undefined",
				error : err
			});
		});
	};
	
	
	// process all BibTeX entries
	var extractBibTeXEntriesAndProcess = function(style, dfd, bibTeXFileContent) {
		
		// get all names
		var entryNames = extractAllBibTeXEntryNames(bibTeXFileContent);

		var entries = [];
		
		// loop through all entryNames
		for(var n = 0; n < entryNames.length; n++) {
			
			// extract a single entry
			var bibTeXEntry = extractBibTeXEntry(bibTeXFileContent, entryNames[n]);
			
			// ... and process it
			document.getElementById("cite-processor").contentWindow.processBibTeXOrDOI(entryNames[n], bibTeXEntry,  style, "bibtex", function(citeData) {
				entries.push({
					cite : citeData.cite,
					citeID : citeData.citeID,
					link : extractURLFromBibTeXEntry(bibTeXEntry),
					reference : citeData.reference
				});
				
				if(entries.length === entryNames.length) {
					dfd.resolve({cites : entries});
				}
			}, function(err) {
				entries.push({
					cite : "undefined",
					citeID : "undefined",
					reference : "undefined",
					error : err
				});
				
				if(entries.length === entryNames.length) {
					dfd.resolve({cites : entries});
				}
			});
		}
	};
	
	
	// extract a BibTeX entry from the provided file content
	var extractBibTeXEntry = function(bibTeXFileContent, ID) {
		var re = new RegExp('@[a-zA-Z ]*{' + ID + ' *,');
		var match = re.exec(bibTeXFileContent);
		if(match) {
			var end = findBalancedEnd(bibTeXFileContent, match.index);
			return bibTeXFileContent.substring(match.index, end + 1);
		}
		else {
			return "";
		}
	};
	
	
	// extract URL from BibTeX entry
	var extractURLFromBibTeXEntry = function(bibTexEntry) {
		var match = bibTexEntry.match(/(url *= *[{|"])(.*?)([}|"])/i);
		if(match) {
			return match[2].trim();
		}
		else {
			return "";
		}
	};
	
	
	// find a balanced end of {}
	var findBalancedEnd = function(str, startIndex) {
		var searchIn = str.substring(startIndex);
		var re = /[\{\}]/g;
		var count = 0;
		var match;
		while ((match = re.exec(searchIn)) != null) {
			if(match[0] == "{") {
				count = count + 1;
			}
			else {
				count = count - 1;
			}
			if(count == 0) {
				return match.index + startIndex;
			}
		}
	};
	
	
	// get all BibTeX entry names
	var extractAllBibTeXEntryNames = function(bibTeXFileContent) {
		var entries = [];
		var matches = bibTeXFileContent.match(/@[a-zA-Z ]*\{.*?,/g);
		for(var n = 0; n < matches.length; n++) {
			entries.push(matches[n].replace(/@.*?\{/, "").replace(",", ""));
		}	
		return entries;
	};
}