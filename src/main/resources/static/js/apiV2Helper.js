/*jshint esversion: 11 */

function APIV2Helper() {

	// IN : contentId
	// OUT: content (of ContentID), content.space
	this.getContentAndSpaceFromId = async function (params) {
		try {
			var contentType = await getContentTypeFromId(params.contentId);
			var mycontent = await getContentOfTypeWithId(params.contentId, contentType);
			var space = contentType === "attachment" ?
				await getAttachmentWithContainerAndSpaceFromId({attachmentId : params.contentId}) :
				await getSpaceFromSpaceId(mycontent.spaceId);
			mycontent.space = space;
			if (params.success) params.success(mycontent);
			return mycontent;
		} catch (err) {
			if (params.error) params.error("An ERROR occured: " + err.name + " in: " + err.text);
			else throw err;
		}
	};


	// IN : contentId
	// OUT: attachment (of ContentID), attachment.containertitle
	this.getAttachmentAndTitleFromId = async function (params) {
		try {
			var attachm = await getContentOfTypeWithId(params.contentId, 'attachment');
			var mypage = await getContentOfTypeWithId(attachm.pageId, 'page');
			attachm.containertitle = mypage.title;
			if (params.success) params.success(attachm);
			return attachm;
		} catch (err) {
			if (params.error) params.error("An ERROR occured: " + err.name + " in: " + err.text);
			else throw err;
		}
	};


	// IN : pageId
	// OUT: attachments of Page in JSON
	this.getAttachmentsforPageId = async function (params) {
		try {
			var attachmRequest = await AP.request({
				"url": "/api/v2/pages/" + params.pageId + "/attachments",
				"type": "GET",
				"dataType": "json",
			});
			var attachm = JSON.parse(attachmRequest.body);
			if (params.success) params.success(attachm);
			return attachm;
		} catch (err) {
			if (params.error) params.error("func:getAttachmentsforPageId() ID:" + params.pageId);
			else throw err;
		}
	};


	// IN : pageId
	// OUT: page, page.space, page.storage
	this.getPageAndStorage = async function (params) {
		try {
			var mypage = await getContentOfTypeWithId(params.pageId, 'page');
			var space = await getSpaceFromSpaceId(mypage.spaceId);
			var mystorage = await getBodyFormat(params.pageId);
			mypage.space = space;
			mypage.storage = mystorage.body.storage.value;
			if (params.success) params.success(mypage);
			return mypage;
		} catch (err) {
			if (params.error) params.error("func:getPageAndStorage() ID:" + params.pageId);
			else throw err;
		}
	};

	var getAttachmentWithContainerAndSpaceFromId = async function (params) {
		try {
			var attachmentRequest = await AP.request("/api/v2/attachments/" + params.attachmentId);
			var attachment = JSON.parse(attachmentRequest.body);

			var containerRequest = await AP.request(attachment.pageId ? "/api/v2/pages/" + attachment.pageId : "/api/v2/blogposts/" + attachment.blogPostId);
			var container = JSON.parse(containerRequest.body);

			var spaceRequest = await AP.request("/api/v2/spaces/" + container.spaceId);
			var space = JSON.parse(spaceRequest.body);

			if (params.success) params.success(space);
			return space;
		}
		catch (error) {
			if (params.error) params.error();
			throw error;
		}
	};

	var getContentTypeFromId = async function (contentId) {
		try {
			var typeRequest = await AP.request({
				"url": "/api/v2/content/convert-ids-to-types",
				"type": "POST",
				"contentType": "application/json; charset=utf-8",
				"dataType": "json",
				"data": JSON.stringify({
					"contentIds": [contentId]
				})
			});
			return JSON.parse(typeRequest.body).results[contentId];
		}
		catch (error) {
			error.text = "func:getContentTypeFromId()";
			throw error;
		}
	};


	var getContentOfTypeWithId = async function (contentId, contentType) {
		try {
			var contentTypeURL;
			switch (contentType) {
				case 'page':
					contentTypeURL = 'pages';
					break;
				case 'blogpost':
					contentTypeURL = 'blogposts';
					break;
				case 'attachment':
					contentTypeURL = 'attachments';
					break;
			}
			var contentRequest = await AP.request({
				"url": "/api/v2/" + contentTypeURL + "/" + contentId,
				"type": "GET",
				"dataType": "json",
			});
			return JSON.parse(contentRequest.body);
		}
		catch (error) {
			error.text = "func:getContentOfTypeWithId() ContentID: " + contentId + " ContentType: " + contentType;
			throw error;
		}
	};


	var getBodyFormat = async function (pageId) {
		try {
			var contentRequest = await AP.request({
				"url": "/api/v2/pages/" + pageId + "?body-format=storage",
				"type": "GET",
				"dataType": "json",
			});
			return JSON.parse(contentRequest.body);
		} catch (error) {
			error.text = "func:getBodyFormat() pageId: " + pageId;
			throw error;
		}
	};


	var getSpaceFromSpaceId = async function (spaceId) {
		try {
			var spacesRequest = await AP.request({
				"url": "/api/v2/spaces/" + spaceId,
				"type": "GET",
				"dataType": "json",
			});
			return JSON.parse(spacesRequest.body);
		} catch (error) {
			error.text = "func:getSpaceFromSpaceId()";
			throw error;
		}
	};
}

var apiV2Helper = new APIV2Helper();