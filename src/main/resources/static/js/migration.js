/*jshint esversion: 9 */

function CiteMigration() {
	
	var processor;
	
	this.init = function(processorReference) {
		processor = processorReference;
				
		$("#simulate").click(function() {
			$("#results").empty();
			doMigrate($("#spaces").val(), true);
		});
		
	
		$("#migrate").click(function() {
			$("#results").empty();
			doMigrate($("#spaces").val(), false);
		});
		
		$("#migration-form").submit(function() {
			return false;
		});
		
		var iframe = document.createElement('iframe');
		iframe.style.display = "none";
		iframe.id="cite-processor";
		iframe.src = "/citation-processor";
		document.body.appendChild(iframe);
	};
	
	
	var doMigrate = async function(spaces, simulate) {
		
		$("#migration-spinner").show();
		$("#simulate,#migrate").hide();

		logInfo("Collecting pages with cites");
		
		var candidates = await getCandidates(spaces);
		
		logInfo("Migration started");
		
		var promises = [];
		candidates.forEach(pageId => promises.push(migratePage(pageId, simulate)));
		await Promise.all(promises);
		
		logInfo("Migration completed");
		$("#migration-spinner").hide();
		$("#simulate,#migrate").show();
	};
	
	
	var getCandidates = async function(spaces) {
		var candidates = [];
		
		try {
			var url = '/rest/api/content/search?limit=50&cql=macro in ("doi-cite","bibtex-cite","single-cite-import","bibtex-listing")';
			if(spaces.length > 0) url += " and space in (" + spaces + ")";
			
			var hasNext = true;
			while(hasNext) {
				var searchRequest = await AP.request(url);
				var searchResults = JSON.parse(searchRequest.body);
				candidates = candidates.concat(searchResults.results.map(result => result.id));
				url = searchResults._links.next || "";
				hasNext = url.length > 0;
			}
		}
		catch(error) {
			logError("Error getting pages: " + error);
		}
		
		return candidates;
	};
	
	
	var migratePage = async function(pageId, simulate) {
		try {
			var pageData = await apiV2Helper.getPageAndStorage({ pageId: pageId });
			var migrationResult = await migrateSource(pageData.storage, pageData.title, pageData.space.key);

			if(migrationResult.errors.length > 0) {
				logError("Error migrating page with id " + pageId + ": " + migrationResult.errors);
			}
			else {
				var noAffected = migrationResult.noDOICites + migrationResult.noBibTeXCites + migrationResult.noImportCites + migrationResult.noBibTeXListings;
				if(noAffected > 0) {
					logInfo("Updating page with id " + pageId + " and " + noAffected + " cites which need a migration");
					if(!simulate) {
						console.log(migrationResult.body);
						var updateResult = await updatePage(pageData, migrationResult.body);
						if(!updateResult.success) {
							logError(updateResult.error);
						}
					}
				}
				else {
					logInfo("Page with id " + pageId + " has no affected cites which need a migration");
				}
			}
		}
		catch(error) {
			logError("Error migrating page with id " + pageId + ": " + error);
		}
	};
	
	
	var migrateSource = async function(source, pageName, spaceKey) {
		
		var conversion = source;
		var replacement;
		var result;
		var errors = "";
		var noDOICites = 0;
		var noBibTeXCites = 0;
		var noImportCites = 0;
		var noBibTeXListings = 0;
		var cite;
		var citeID;
		var addLinkType;
		
		var format = getMiddlePart(source, 'ac:name="format">', '<', 'ieee');
		var override = getMiddlePart(source, 'ac:name="citationOverride">', '<', 'false');
			
		var doiCiteRegEx = /<ac:structured-macro ac:name="doi-cite".*?ac:structured-macro>/gs;
		var doiCite;
		while((doiCite = doiCiteRegEx.exec(source)) !== null) {
			if(doiCite[0].indexOf("compiledCite") === -1) {
				noDOICites++;
				citeID = unescapeHtml(getMiddlePart(doiCite[0], 'ac:name="citeID">', '<', ''));
				cite = await processor.compileDOICite(citeID, format);
				addLinkType = getMiddlePart(doiCite[0], 'ac:name="addLink">', '<', 'false') === "true" ? 3 : 0;
				if(!cite.error) {
					replacement = addParameterToMacroFragment(doiCite[0], "compiledCite", cite.cite);
					replacement = addParameterToMacroFragment(replacement, "compiledReference", cite.reference);
					replacement = addParameterToMacroFragment(replacement, "addedLink", addLinkType > 0 ? cite.link : "");
					replacement = addParameterToMacroFragment(replacement, "cslReference", override);
					replacement = addParameterToMacroFragment(replacement, "style", format);
					replacement = addParameterToMacroFragment(replacement, "addLinkType", addLinkType);
					conversion = conversion.replace(doiCite[0], replacement);
				}
				else {
					errors += "error converting DOI cite with id " + citeID + ": " + result.error + "; ";
				}
			}
		}
		source = conversion;
		
		var bibTeXCiteRegEx = /<ac:structured-macro ac:name="bibtex-cite".*?ac:structured-macro>/gs;
		var bibTeXCite;
		while((bibTeXCite = bibTeXCiteRegEx.exec(source)) !== null) {
			if(bibTeXCite[0].indexOf("compiledCite") === -1) {
				noBibTeXCites++;
				result = await pageAndAttachmentResolver(bibTeXCite[0], pageName, spaceKey);
				if(result.success) {
					citeID = unescapeHtml(getMiddlePart(bibTeXCite[0], 'ac:name="citeID">', '<', ''));
					cite = await processor.compileBibTeXCite(citeID, format, result.url, true);
					addLinkType = getMiddlePart(bibTeXCite[0], 'ac:name="includeLink">', '<', 'false') === "true" ? 4 : 0;
					if(!cite.error) {
						replacement = addParameterToMacroFragment(bibTeXCite[0], "compiledCite", cite.cite);
						replacement = addParameterToMacroFragment(replacement, "compiledReference", cite.reference);
						replacement = addParameterToMacroFragment(replacement, "addedLink", addLinkType > 0 ? cite.link : "");
						replacement = addParameterToMacroFragment(replacement, "cslReference", override);
						replacement = addParameterToMacroFragment(replacement, "style", format);
						replacement = addParameterToMacroFragment(replacement, "bibTeXFileID", result.id);
						replacement = addParameterToMacroFragment(replacement, "addLinkType", addLinkType);
						conversion = conversion.replace(bibTeXCite[0], replacement);
					}
					else {
						errors += "error converting BibTeX cite with id " + citeID + ": " + cite.error + "; ";			
					}
				}
				else {
					errors += "error in converting BibTeX cite: " + result.error + "; ";
				}
			}
		}
		source = conversion;
		
		var bibTeXListingRegEx = /<ac:structured-macro ac:name="bibtex-listing".*?ac:structured-macro>/gs;
		var bibTeXListing;
		while((bibTeXListing = bibTeXListingRegEx.exec(source)) !== null) {
			if(bibTeXListing[0].indexOf("compiledCite") === -1) {
				noBibTeXListings++;
				result = await pageAndAttachmentResolver(bibTeXListing[0], pageName, spaceKey);
				if(result.success) {
					var listingFormat = getMiddlePart(bibTeXListing[0], 'ac:name="format">', '<', 'ieee');
					var listing = await processor.compileBibTeXListing(listingFormat, result.url, true);
					addLinkType = getMiddlePart(bibTeXListing[0], 'ac:name="includeLink">', '<', 'false') === "true" ? 4 : 0;
					if(!listing.error) {
						replacement = addParameterToMacroFragment(bibTeXListing[0], "compiledCite", JSON.stringify(listing.cites));
						replacement = addParameterToMacroFragment(replacement, "style", format);
						replacement = addParameterToMacroFragment(replacement, "bibTeXFileID", result.id);
						replacement = addParameterToMacroFragment(replacement, "addLinkType", addLinkType);
						conversion = conversion.replace(bibTeXListing[0], replacement);
					}
					else {
						errors += "error converting BibTeX listing: " + listing.error + "; ";			
					}
				}
				else {
					errors += "error in converting BibTeX listing: " + result.error + "; ";
				}
			}
		}
		source = conversion;
		
		var importCiteRegEx = /<ac:structured-macro ac:name="single-cite-import".*?ac:structured-macro>/gs;
		var importCite;
		while((importCite = importCiteRegEx.exec(source)) !== null) {
			if(importCite[0].indexOf("importFrom") === -1) {
				noImportCites++;
				result = await pageAndAttachmentResolver(importCite[0], pageName, spaceKey);
				if(result.success) {
					replacement = addParameterToMacroFragment(importCite[0], "importFrom", result.id);
					conversion = conversion.replace(importCite[0], replacement);
				}
				else {
					errors += "error in converting imported cite: " + result.error + "; ";
				}
			}
		}
		
		return {
			body : conversion,
			errors : errors,
			noDOICites : noDOICites,
			noBibTeXCites : noBibTeXCites,
			noImportCites : noImportCites,
			noBibTeXListings : noBibTeXListings
		};
	};
	
	
	var getMiddlePart = function(fragment, preface, postface, defaultValue) {
		var mpRegEx = new RegExp("(" + preface + ")(.*?)(" + postface + ")", "gs");
		var mpHit;
		if((mpHit = mpRegEx.exec(fragment)) !== null) {
			return mpHit[2] || defaultValue;
		}
		else {
			return defaultValue;
		}
	};
	
	
	var pageAndAttachmentResolver = function(fragment, currentPageName, currentSpaceKey) {
		const parser = new DOMParser();
		
		try {
			const fragmentDoc = parser.parseFromString(fragment, "text/html");
			
			var pageName = $(fragmentDoc).find('ac\\:parameter[ac\\:name="page"]').length > 0 ?
				$(fragmentDoc).find('ac\\:parameter[ac\\:name="page"]').find('ri\\:page').length > 0 ? $(fragmentDoc).find('ac\\:parameter[ac\\:name="page"]').find('ri\\:page').attr('ri:content-title') : $(fragmentDoc).find('ac\\:parameter[ac\\:name="page"]').text() :
				currentPageName;
			var spaceKey = $(fragmentDoc).find('ac\\:parameter[ac\\:name="space"]').length > 0 ?
				$(fragmentDoc).find('ac\\:parameter[ac\\:name="space"]').find('ri\\:space').length > 0 ? $(fragmentDoc).find('ac\\:parameter[ac\\:name="space"]').find('ri\\:space').attr('ri:space-key') : $(fragmentDoc).find('ac\\:parameter[ac\\:name="space"]').text() :
				currentSpaceKey;
			var fileName = $(fragmentDoc).find('ac\\:parameter[ac\\:name="name"]').length > 0 ?
				$(fragmentDoc).find('ac\\:parameter[ac\\:name="name"]').find('ri\\:attachment').length > 0 ? $(fragmentDoc).find('ac\\:parameter[ac\\:name="name"]').find('ri\\:attachment').attr('ri:filename') : $(fragmentDoc).find('ac\\:parameter[ac\\:name="name"]').text() :
				undefined;
			
			if(fileName) {
				
				// AP-528 related code for customers in which AttachmentResourceIdentifier is used
				if(fileName.startsWith("AttachmentResourceIdentifier")) {
					let fields = extractFields(fileName);
					console.warn("AttachmentResourceIdentifier has been used");
					console.warn(fields);
					fileName = fields.filename;
					pageName = fields.title || pageName;
					spaceKey = fields.spaceKey || spaceKey;
				}
				
				return new Promise((resolve, reject) => {
					AP.request({
						url: "/rest/api/content/search?cql=title='" + encodeURIComponent(fileName) + "'&expand=container,space&limit=100",
						success: function(queryText) {
							var queryJSON = JSON.parse(queryText);
							for(var n = 0; n < queryJSON.results.length; n++) {
								var att = queryJSON.results[n];
								if(att.space.key === spaceKey && att.container.title === pageName) {
									return resolve({
										success : true,
										type: "attachment",
										id: att.id,
										url: att._links.download
									});
								}
							}
							return resolve({
								success : false,
								error: "not found"
							});
						},
						error: function(request, status, error) {
							return resolve({
								success : false,
								error: buildErrorMessage(request, status, error)
							});
						}
					});
				});
			}
			else if(pageName) {
				return new Promise((resolve, reject) => {
					AP.request({
						url: "/rest/api/content/search?cql=title='" + encodeURIComponent(pageName) + "' and space=" + spaceKey,
						success: function(queryText) {
							var queryJSON = JSON.parse(queryText);
							if(queryJSON.results.length === 1) {
								var page = queryJSON.results[0];
								return resolve({
									success : true,
									type: "page",
									id: page.id,
								});
							}
							else {
								return resolve({
									success : false,
									error: "not found"
								});
							}
						},
						error: function(request, status, error) {
							return resolve({
								success : false,
								error: buildErrorMessage(request, status, error)
							});
						}
					});
				});
			}
			else {
				console.error(fragment);
				return {
					success : false,
					error: "unknown source"
				};
			}
		}
		catch(error) {
			console.error(fragment);
			return {
				success : false,
				error: error
			};
		}
	};
	
	
	var addParameterToMacroFragment = function(fragment, parameterName, parameterValue) {
		return fragment.replace("</ac:structured-macro>", '<ac:parameter ac:name="' + parameterName + '">' + sanitizeText(parameterValue) + '</ac:parameter></ac:structured-macro>');
	};
	
	
	var updatePage = function(page, newBody) {
		var payload = {
			id : page.id,
			title : page.title,
			type : "page",
			status : "current",
			version : {
				number : page.version.number + 1,
				message : "Simple Cite migration"
			},
			body : {
				storage: {
					value: newBody,
					representation: "storage"
				}
			}
		};
		return new Promise((resolve, reject) => {
			AP.request({
				url: "/api/v2/pages/" + page.id, 
				type: "PUT",
				data: JSON.stringify(payload),
				contentType: "application/json",
				success: function() {
					resolve({
						success: true
					});
				},
				error: function(request, status, error) {
					resolve({
						success: false,
						error: buildErrorMessage(request, status, error)
					});
				}
			});
		});
	};
	
	
	var sanitizeText = function(text) {
		return $('<div/>').text(text).html();
	};
	
	
	var unescapeHtml = function(encodedStr) {
		return $("<div/>").html(encodedStr).text();
	};
	
	
	var buildErrorMessage = function(request, status, error) {
		return request.status + ";" + status + ";" + error;
	};
	
	
	var logError = function(message) {
		console.error(message);
		addToResults(message, "#c0626f");
	};
	
	
	var logInfo = function(message) {
		console.log(message);
		addToResults(message, "inherit");
	};
	
	
	var addToResults = function(message, color) {
		// security hint: color is only defined within this JS file, the message could contain malicious code and is therefore escaped
		$("#results").append('<p style="color:' + color + '">' + sanitizeText(message) + '</p>');
	};
	
	
	// AP-528 related code for customers in which AttachmentResourceIdentifier is used
	var extractFields = function(input) {
		  // Flatten the structure by removing nested class names and brackets
	    let flattened = input.replace(/([A-Za-z0-9=]+\[)|\]/g, '');
	
	    // Split the string into potential key-value pairs
	    let potentialPairs = flattened.split(/, ?(?=\w+=)/);
	    let keyValuePairs = {};
	
	    // Process each potential pair
	    potentialPairs.forEach(pair => {
	        let [key, value] = pair.split(/=(.+)/); // Split only on the first '='
	        if (key && value) {
	            keyValuePairs[key.trim()] = value.trim();
	        }
	    });
	
	    return keyValuePairs;
	};
}


var citeMigration = new CiteMigration();
var processor = new CiteProcessor();	
citeMigration.init(processor);