package de.edrup.confluence.plugins.simplecite.util;

import java.util.LinkedHashMap;
import java.util.concurrent.CompletableFuture;

public class CiteMapWrapper {
	
	private CompletableFuture<LinkedHashMap<String, CiteData>> citeMap = new CompletableFuture<LinkedHashMap<String, CiteData>>();
	
	public CiteMapWrapper(CompletableFuture<LinkedHashMap<String, CiteData>> citeMap) {
		this.citeMap = citeMap;
	}
	
	CompletableFuture<LinkedHashMap<String, CiteData>> getCiteMap() {
		return citeMap;
	}
}
