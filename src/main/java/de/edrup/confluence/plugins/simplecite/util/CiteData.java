package de.edrup.confluence.plugins.simplecite.util;


public class CiteData {
	
	// data
	private Integer number = 0;
	private String citeText = "";
	private String macroIDs = "";
	private Integer count = 0;
	private String pageLink = "";
	
	
	public CiteData() {
	}
	
	
	// init as clone
	public CiteData(CiteData clone) {
		number = clone.number;
		citeText = clone.citeText;
		macroIDs = clone.macroIDs;
		count = clone.count;
		pageLink = clone.pageLink;
	}
	
	
	// getter and setter functions
	public Integer getNumber() {
		return number;
	}
	
	
	public void setNumber(Integer number) {
		this.number = number;
	}
	
	
	public String getCiteText() {
		return citeText;
	}
	
	
	public void setCiteText(String citeText) {
		this.citeText = citeText;
	}
	
	
	public void addMacroID(String macroID) {
		macroIDs = macroIDs + macroID + ";";
		count = count + 1;
	}
	
	
	public String getMacroIDs() {
		return macroIDs;
	}
		
	
	public Integer getCount() {
		return count;
	}
	
	
	public void setPageLink(String pageLink) {
		this.pageLink = pageLink;
	}
	
	
	public String getPageLink() {
		return pageLink;
	}
	
	
	// get the cite count based on the macroID 
	public Integer getCountForMacroID(String macroID) {
		if(macroIDs.contains(macroID)) {
			return macroIDs.substring(0, macroIDs.indexOf(macroID) + macroID.length()).split(";").length;
		}
		else {
			return 0;
		}
	}
	
	
	// weave in another CiteData object
	public void weaveIn(CiteData partner, String importIdent) {
		
		if(!citeText.contains("<p>") && !citeText.isEmpty()) {
			citeText =  "<p>" + citeText + "</p>";
		}
		if(!citeText.contains("<p>" + partner.getCiteText() + "</p>")) {
			citeText = citeText + "<p>" + partner.getCiteText() + "</p>";
		}
		
		citeText = citeText.replaceFirst("<p>" + importIdent + ".*?<\\/p>", "");

		macroIDs = macroIDs + partner.getMacroIDs();
		
		if(!pageLink.contains("<p>")) {
			pageLink = "<p>" + pageLink + "</p>";
		}
		pageLink = pageLink + "<p>" + partner.getPageLink() + "</p>";
	}
}
