package de.edrup.confluence.plugins.simplecite.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.util.HtmlUtils;

import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.ContextJwt;
import com.atlassian.connect.spring.IgnoreJwt;

import de.edrup.confluence.plugins.simplecite.util.CiteProcessor;


@Controller
public class SimpleCite {
		
	@Autowired
	private CiteProcessor citeProc;
	
	@Value("${SECRET}")
    private String secret;
	
	// all cites
	@RequestMapping(value = {"/single-cite", "/short-cite", "/single-cite-import", "/doi-cite", "/bibtex-cite", "/dbibtex-cite", "/single-cite-plain"}, method = RequestMethod.GET)
	public ResponseEntity<String> singleCite(@AuthenticationPrincipal AtlassianHostUser hostUser,
		@RequestParam(value="macroID") String macroID,
		@RequestParam(value="pageID", defaultValue="") String pageId,
		@RequestParam(value="pageVersion") String pageVersion,
		@RequestParam(value="outputType", defaultValue="") String outputType,
		@RequestParam(value="citeID") String citeID,
		@RequestParam(value="pageNumbers") String pageNumbers,
		@RequestParam(value="superscript", defaultValue="true") String superscript,
		@RequestParam(value="cslReference", defaultValue="false") String cslReference,
		@RequestParam(value="defineOnly", defaultValue="false") String defineOnly,
		@RequestParam(value="compiledReference", defaultValue="") String compiledReference,
		@RequestParam(value="bibOutput", defaultValue="") String bibOutput,
		@RequestParam(value="pages", defaultValue="") String pages) {
		
		// return bad request in vase the pageId is not provided
		if(pageId.isEmpty()) {
			return new ResponseEntity<String>("", HttpStatus.BAD_REQUEST);
		}

		// compile the cite
		if(pageNumbers.isEmpty() && !pages.isEmpty()) pageNumbers = pages;
		String content = citeProc.compileCite(hostUser.getHost().getBaseUrl(), pageId, pageVersion, outputType, HtmlUtils.htmlEscape(citeID), macroID, HtmlUtils.htmlEscape(pageNumbers), superscript, defineOnly, cslReference, HtmlUtils.htmlEscape(compiledReference), bibOutput);
		
		// prepare the response
		HttpHeaders rHeaders= new HttpHeaders();
		if(!content.contains("ERROR:")) {
			rHeaders.setCacheControl("max-age=93600, s-maxage=93600");
		}
		
		return new ResponseEntity<String>(content, rHeaders, HttpStatus.OK);
	}
	
	
	// cite summary
	@RequestMapping(value = "/cite-summary", method = RequestMethod.GET)
	public ResponseEntity<String> citeSummary(@AuthenticationPrincipal AtlassianHostUser hostUser,
		@RequestParam(value="pageID", defaultValue="") String pageId,
		@RequestParam(value="pageVersion") String pageVersion,
		@RequestParam(value="lic", defaultValue="") String lic,
		@RequestParam(value="outputType", defaultValue="") String outputType) {
		
		// return bad request in vase the pageId is not provided
		if(pageId.isEmpty()) {
			return new ResponseEntity<String>("", HttpStatus.BAD_REQUEST);
		}
		
		// compile the cite summary
		String content = (lic.equals("active") || hostUser.getHost().getBaseUrl().contains("edrup-dev")) ? citeProc.compileSummary(hostUser.getHost().getBaseUrl(), pageId, pageVersion, hostUser.getUserAccountId().orElse(""), outputType) : citeProc.getNoLicenseOutput();
		
		// prepare the response
		HttpHeaders rHeaders= new HttpHeaders();
		if(content.contains("<ol>")) {
			rHeaders.setCacheControl("max-age=93600, s-maxage=93600");
		}
		
		return new ResponseEntity<String>(content, rHeaders, HttpStatus.OK);
	}
	
	
	// space cite summary
	@RequestMapping(value = "/space-cite-summary", method = RequestMethod.GET)
	@ResponseBody
	public String spaceCiteSummary(@AuthenticationPrincipal AtlassianHostUser hostUser,
		@RequestParam(value="spaces", defaultValue="") String spaces,
		@RequestParam(value="currentSpace") String currentSpace,
		@RequestParam(value="lic", defaultValue="") String lic) {
		
		return (lic.equals("active") || hostUser.getHost().getBaseUrl().contains("edrup-dev")) ? citeProc.compileSpaceCiteSummary(hostUser.getHost().getBaseUrl(), HtmlUtils.htmlEscape(spaces), currentSpace) : citeProc.getNoLicenseOutput();
	}
	
	
	// BibTeX listing
	@RequestMapping(value = "/bibtex-listing", method = RequestMethod.GET)
	public ResponseEntity<String> bibTexListing(@AuthenticationPrincipal AtlassianHostUser hostUser,
		@RequestParam(value="pageID", defaultValue="") String pageId,
		@RequestParam(value="pageVersion") String pageVersion,
		@RequestParam(value="macroID") String macroID,
		@RequestParam(value="uuid") String uuid,
		@RequestParam(value="lic", defaultValue="") String lic) {
		
		String content = (lic.equals("active") || hostUser.getHost().getBaseUrl().contains("edrup-dev")) ? citeProc.compileBibTexListing(hostUser.getHost().getBaseUrl(), pageId, macroID, uuid) : citeProc.getNoLicenseOutput();
		
		// prepare the response
		HttpHeaders rHeaders= new HttpHeaders();
		if(content.contains("<ol>")) {
			rHeaders.setCacheControl("max-age=93600, s-maxage=93600");
		}
		
		return new ResponseEntity<String>(content, rHeaders, HttpStatus.OK);
	}
	
	
	// cite editor
	@RequestMapping(value = "/cite-editor", method = RequestMethod.GET)
	public ModelAndView citeEditor(@AuthenticationPrincipal AtlassianHostUser hostUser, 
		@RequestParam(value="caller", defaultValue="single-cite") String caller, HttpServletRequest request) {
				
		// prepare the model
	    ModelAndView model = new ModelAndView("cite-editor");
	    model.addObject("baseUrl", hostUser.getHost().getBaseUrl());
	    model.addObject("caller", caller);
	    
	    // and return it
	    return model;
	}
	
	
	// citation processor
	@RequestMapping(value = "/citation-processor", method = RequestMethod.GET)
	@IgnoreJwt
	public ModelAndView citationProcessor() {				
	    ModelAndView model = new ModelAndView("citation-processor");
	    return model;
	}
	
	
	// migration UI
	@RequestMapping(value = "/migration", method = RequestMethod.GET)
	public ModelAndView migration() {				
	    ModelAndView model = new ModelAndView("migration");
	    return model;
	}
	
	
	// load an external file
	// security remarks: the content of the external file is processed by JS on the client side - a security focused escaping is not necessary here
	// it is the intention to allow all kinds of URLs here (white listing is not an option); however we want to avoid that someone tries to approach this server
	@RequestMapping(value = "/external", method = RequestMethod.GET)
	@ContextJwt
	public ResponseEntity<String> external(@RequestParam(value="url") String url, HttpServletResponse sResponse) {
		try {
			if(isItMe(url) || !url.startsWith("https://")) {
				return new ResponseEntity<String>("", HttpStatus.FORBIDDEN);
			}
			HttpsURLConnection con = (HttpsURLConnection) new URL(url).openConnection();
			con.setRequestMethod("GET");
			int responseCode = con.getResponseCode();
			if(responseCode == HttpURLConnection.HTTP_OK) {
				BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
				String inputLine;
				StringBuffer response = new StringBuffer();
				while ((inputLine = in.readLine()) != null) {
					response.append(inputLine);
				}
				in.close();
				sResponse.setHeader("Content-Disposition", "attachment");
				return new ResponseEntity<String>(response.toString(), HttpStatus.OK);
			}
			else {
				System.out.printf("ERROR: error loading external file: %d%n", responseCode);
				return new ResponseEntity<String>("", HttpStatus.valueOf(responseCode));
			}
		}
		catch(Exception e) {
			System.out.printf("ERROR: error loading external file: %s%n", e.toString());
			return new ResponseEntity<String>("", HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	
	// add data to preview cache
	// remark: currently used for the BibTeX listing only
	@RequestMapping(value = "/cache", method = RequestMethod.POST)
	@ContextJwt
	public ResponseEntity<String> cache(@RequestParam(value="uuid") String uuid, @RequestBody String payload) {
		citeProc.putToPreviewCache(uuid, payload);
		return new ResponseEntity<String>("", HttpStatus.OK);
	}
	
	
	// check if url refers back to this instance
    private boolean isItMe(String url) {
        try {
        	InetAddress addr = InetAddress.getByName(new URL(url).getHost());
            if(addr.isAnyLocalAddress() || addr.isLoopbackAddress() || addr.equals(InetAddress.getByName("127.0.0.1"))) return true;
            return NetworkInterface.getByInetAddress(addr) != null;
        }
        catch (Exception e) {
            return false;
        }
    }
}
