package de.edrup.confluence.plugins.simplecite.util;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map.Entry;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadLocalRandom;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.safety.Safelist;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.task.TaskExecutor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.web.util.HtmlUtils;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring6.SpringTemplateEngine;

import com.atlassian.connect.spring.AtlassianHostRestClients;

@Service
public class CiteProcessor {
	
	@Autowired
	private AtlassianHostRestClients atlassianHostRestClients;
	
	@Autowired
	private MessageSource messageSource;
	
	@Autowired
    private SpringTemplateEngine templateEngine;
	
	@Autowired
	TaskExecutor executor;
	
	class ContentAndVersion {
		public String content = "";
		public String version = "";
	}
	
	final String importIdent = "imported from page id: ";
	
	private ConcurrentHashMap<String, CiteMapWrapper> newCache = new ConcurrentHashMap<String, CiteMapWrapper>();
	private ConcurrentHashMap<String, String> previewCache = new ConcurrentHashMap<String, String>();
	
	
	// compile the cite in Confluence storage format
	public String compileCite(String cCLink, String pageId, String pageVersion, String outputType, String citeID, String macroID, String pageNumbers, String superscript, String defineOnly, String cslReference, String compiledReference, String bibOutput) {
		
		// a define only cite does not have a visual output
		if(defineOnly.equals("true")) {
			return "";
		}

		LinkedHashMap<String, CiteData> cites = getCiteMap(cCLink, pageId, pageVersion, null);
		
		try {
			String citeStorage = "";
			if(bibOutput.equals("true")) {
				citeStorage = "<b>" + citeID + "</b>: " + cites.get(citeID).getCiteText();
			}
			else {
				// start with anchor
				citeStorage = String.format("<ac:structured-macro ac:name=\"anchor\"><ac:parameter ac:name=\"\">SingleCite_%d_%d</ac:parameter></ac:structured-macro>",
					cites.get(citeID).getNumber(), cites.get(citeID).getCountForMacroID(macroID));
				
				// add cite with link to cite summary
				citeStorage = citeStorage + String.format("<ac:link ac:anchor=\"CiteSummary_%d\"><ac:link-body>", cites.get(citeID).getNumber());
				boolean useCompiledReference = cslReference.equals("true") && (compiledReference.length() > 3);
				if(useCompiledReference) {
					citeStorage += compiledReference;
				}
				else {
					citeStorage = citeStorage + "[" + cites.get(citeID).getNumber().toString();
					if(!pageNumbers.isEmpty()) {
						citeStorage = citeStorage + (Character.isDigit(pageNumbers.charAt(0)) ? ", p. " : ", ") + pageNumbers;
					}
					citeStorage = citeStorage + "]";
				}
				if(superscript.equals("true") && !useCompiledReference) {
					citeStorage = "<sup>" + citeStorage + "</sup>"; 
				}
				citeStorage = citeStorage + "</ac:link-body></ac:link>";
			}
			
			return citeStorage;
		}
		
		catch(Exception e) {
			newCache.remove(generateCacheKey(cCLink, pageId, pageVersion));
			if(outputType.equals("preview")) {
				return messageSource.getMessage("simple-cite-cloud.message.citeProcessingFailedPreview", null, Locale.getDefault());
			}
			else {
				System.out.printf("ERROR: %s: error processing cite: %s%n", cCLink, e.toString());
				//System.out.println(getStackTrace(e));
				return messageSource.getMessage("simple-cite-cloud.message.citeProcessingFailed", null, Locale.getDefault());
			}
		}
	}
	
	
	// compile the cite summary in Confluence storage format
	public String compileSummary(String cCLink, String pageId, String pageVersion, String userKey, String outputType) {
			
		try {
		
			LinkedHashMap<String, CiteData> cites = getCiteMap(cCLink, pageId, pageVersion, null);
			
			// list empty?
			if(cites.isEmpty()) {
				return messageSource.getMessage("simple-cite-cloud.message.noCites", null, Locale.getDefault());
			}
			
			Iterator<Entry<String, CiteData>> it = cites.entrySet().iterator();
			
			// prepare the list output
			String summary = cites.size() < 100 ? "<ol>" : "";
			
			// loop through all cites
			while(it.hasNext()) {
				
				Entry<String, CiteData> citeEntry = it.next();
				CiteData cite = new CiteData(citeEntry.getValue());
				
				// resolve imported cite
				if(cite.getCiteText().contains(importIdent)) {
					
					String importPageId = cite.getCiteText().replace(importIdent, "");
					
					// get the cite data of the page we are importing from
					String pageVersionReplacement = Long.toString(new Date().getTime() / 2000L);
					LinkedHashMap<String, CiteData> importedCites = getCiteMap(cCLink, importPageId, pageVersionReplacement, null);
					
					// change our cite text to the one from the page we are importing from
					if(importedCites.containsKey(citeEntry.getKey())) {
						cite.setCiteText(importedCites.get(citeEntry.getKey()).getCiteText());
					}
					else {
						cite.setCiteText(messageSource.getMessage("simple-cite-cloud.message.import.citeNotFound", null, Locale.getDefault()));
					}
				}
				
				// start list entry
				summary += cites.size() < 100 ? "<li>" : ("<p>(" + cite.getNumber() + ") ");
				
				// get the cite
				String citeText = cite.getCiteText();
				
				// add anchor
				summary += String.format("<ac:structured-macro ac:name=\"anchor\"><ac:parameter ac:name=\"\">CiteSummary_%d</ac:parameter></ac:structured-macro>", cite.getNumber());
				
				// add an arrow in case there is more than one reference to the citeID
				if(cite.getCount() > 1) {
					summary += "^ ";
				}
				
				// loop through all references
				for(int n = 0; n < cite.getCount(); n++) {
					
					// attach the link to the arrow in case we have only have one reference
					if(cite.getCount() == 1) {
						summary += String.format("<ac:link ac:anchor=\"SingleCite_%d_1\"><ac:link-body>^ </ac:link-body></ac:link>",
							cite.getNumber());
					}
					
					// add a number for each reference and the corresponding link
					else {
						summary += String.format("<ac:link ac:anchor=\"SingleCite_%d_%d\"><ac:link-body><sup>%d </sup></ac:link-body></ac:link>",
							cite.getNumber(), n + 1, n + 1);
					}
				}
				
				// insert userKey in the link to make the call on behalf of the user later
				if(citeText.contains("link-to-attachment")) {
					System.out.println(citeText);
					citeText = citeText.replace("&cCLink=", "&userKey=" + userKey + "&cCLink=");
				}
				
				// add the cite text and close the list entry
				// remark: per definition the citeText can contain HTML/XML - malicious parts however are not displayed by Confluence
				summary += citeText;
										
				summary +=  cites.size() < 100 ? "</li>" : "</p>";
			}
			
			// close the list
			summary +=  cites.size() < 100 ? "</ol>" : "";
			
			// add a note in preview mode
			if("preview".equals(outputType)) {
				summary += "<p>" + messageSource.getMessage("simple-cite-cloud.message.reloadPage", null, Locale.getDefault()) + "</p>";
			}
			
			// return the list of cites
			return summary;	
		}
		catch(Exception e) {
			newCache.remove(generateCacheKey(cCLink, pageId, pageVersion));
			System.out.printf("ERROR: %s: error processing cite summary: %s%n", cCLink, e.toString());
			System.out.println(getStackTrace(e));
			return messageSource.getMessage("simple-cite-cloud.message.citeSummaryProcessingFailed", null, Locale.getDefault());
		}
	}
	
	
	// compile the space cite summary on Confluence storage format
	public String compileSpaceCiteSummary(String cCLink, String spaces, String currentSpace) {
		
		// perform the search for all pages containing single-cites
		String searchResult = searchCites(cCLink, spaces, currentSpace);
		if(searchResult == "") {
			return  messageSource.getMessage("simple-cite-cloud.message.searchFailed", null, Locale.getDefault());
		}
		
		// build a cite map containing all cites of the pages we found
		try {
			LinkedHashMap<String, CiteData> spaceCiteMap = new LinkedHashMap<String, CiteData>();
			
			JSONArray results = new JSONObject(searchResult).getJSONArray("results");
			
			// loop through all pages we found
			for(int n = 0; n < results.length(); n++) {
								
				// get the cite map of our page
				LinkedHashMap<String, CiteData> pageCiteMap = getCiteMap(cCLink, results.getJSONObject(n).getString("id"),
					Integer.valueOf(results.getJSONObject(n).getJSONObject("version").getInt("number")).toString(),
					results.getJSONObject(n).getJSONObject("body").getJSONObject("storage").getString("value"));
				
				// prepare an iterator
				Iterator<Entry<String, CiteData>> it = pageCiteMap.entrySet().iterator();
				
				// loop through all cites of the page
				while(it.hasNext()) {
					Entry<String, CiteData> pageCiteEntry = it.next();
					String citeID = pageCiteEntry.getKey();
					
					// get the cite data and add the link to the page
					CiteData pageCiteData = pageCiteEntry.getValue();
					String linkToPage = String.format("<a href=\"%s/pages/viewpage.action?pageId=%s\">%s</a>", cCLink, results.getJSONObject(n).getString("id"), HtmlUtils.htmlEscape(results.getJSONObject(n).getString("title")));
					pageCiteData.setPageLink(linkToPage);
					
					// do we already have that citeID in our space-wide-list?
					if(spaceCiteMap.containsKey(citeID)) {
						
						// get the corresponding space cite data
						CiteData spaceCiteData = spaceCiteMap.get(citeID);
						
						// weave in our page cite data
						spaceCiteData.weaveIn(pageCiteData, importIdent);
						
						// and put it back in the space wide list
						spaceCiteMap.put(citeID, spaceCiteData);
					}
					
					// in case we don't have that citeID
					else {
						// clone the page cite data and store them in the list
						CiteData pageCiteDataCloned = new CiteData(pageCiteData);
						spaceCiteMap.put(citeID, pageCiteDataCloned);
					}
				}
			}
			
			// start building the output
			String spaceCiteSummary = messageSource.getMessage("simple-cite-cloud.spaceCiteSummaryHeadings", null, Locale.getDefault());
			
			// prepare an iterator
			Iterator<Entry<String, CiteData>> it = spaceCiteMap.entrySet().iterator();
			
			// loop through all cites we found and add them to the table
			while(it.hasNext()) {
				Entry<String, CiteData> spaceCiteEntry = it.next();
				
				// security remark: the cite text and the page link can contain XML in Confluence storage format 
				spaceCiteSummary = spaceCiteSummary + String.format("<tr><td>%s</td><td>%s</td><td>%s</td></tr>",
					HtmlUtils.htmlEscape(spaceCiteEntry.getKey()),
					spaceCiteEntry.getValue().getCiteText(),
					spaceCiteEntry.getValue().getPageLink());
			}
			
			spaceCiteSummary = spaceCiteSummary + "</tbody></table>";
			
			return spaceCiteSummary;
		}
		
		catch(Exception e) {
			System.out.printf("ERROR: %s: error processing space cite summary: %s%n", cCLink, e.toString());
			return  messageSource.getMessage("simple-cite-cloud.message.spaceCiteSummaryProcessingFailed", null, Locale.getDefault());
		}
	}
	
	
	// compile a BibTeXListing
	public String compileBibTexListing(String baseUrl, String pageId, String macroId, String uuid) {
		try {
			String content = getPageContentAndVersion(baseUrl, pageId).content;
			String macroSnippet = HtmlUtils.htmlUnescape(getMiddlePart(content, macroId, "</ac:structured-macro", false));
			JSONArray cites = new JSONArray(content.contains(macroId) ? getParameter(macroSnippet, "compiledCite") : previewCache.getOrDefault(uuid, ""));
			previewCache.remove(uuid);
			
		    Context ctx = new Context();
		    ctx.setVariable("cites", cites);
			return templateEngine.process("bibtex-listing", ctx);
		}
		catch(Exception e) {
			System.out.printf("ERROR: %s: error processing BibTeX listing: %s%n", baseUrl, e.toString());
			return messageSource.getMessage("simple-cite-cloud.message.bibTeXListingProcessingFailed", null, Locale.getDefault()) + ": " + e.toString();			
		}
	}
	
	
	// store in previewCache
	public void putToPreviewCache(String uuid, String preview) {
		previewCache.put(uuid, preview);
	}
	
	
	// get a map of cites from the specified page
	private LinkedHashMap<String, CiteData> getCiteMap(String cCLink, String pageId, String pageVersion, String presentContent) {
				
		// create the cache key
		String cacheKey = generateCacheKey(cCLink, pageId, pageVersion);
		
		// break too synchronous requests
		int randomNum = ThreadLocalRandom.current().nextInt(10, 400 + 1);
		try {
			Thread.sleep(randomNum);
		}
		catch(Exception e) {}
		
		// check whether we already have the page in the cache
		try {
			if(newCache.get(cacheKey) != null) {
				return newCache.get(cacheKey).getCiteMap().get(12500, TimeUnit.MILLISECONDS);
			}
		}
		catch(Exception e) {
			System.out.printf("ERROR: cite map in cache for %s and %s was not available in the timeout interval%n", cCLink, pageId);
			return new LinkedHashMap<String, CiteData>();
		}
		
		SecurityContext securityContext = SecurityContextHolder.getContext();
		newCache.putIfAbsent(cacheKey, new CiteMapWrapper(CompletableFuture.supplyAsync(() -> prepareNewCiteMap(securityContext, cCLink, pageId, pageVersion, presentContent), executor)));
		
		try {
			return newCache.get(cacheKey).getCiteMap().get(12000, TimeUnit.MILLISECONDS);
		}
		catch(Exception e) {
			System.out.printf("ERROR: could not create cite map for %s and %s within the timeout interval%n", cCLink, pageId);
			return new LinkedHashMap<String, CiteData>();
		}
	}
	
	
	private LinkedHashMap<String, CiteData> prepareNewCiteMap(SecurityContext securityContext, String cCLink, String pageId, String pageVersion, String presentContent) {
		
		final String macroIDMatchPre = "ac:macro-id=\"";
		final String macroIDMatchPost = "\"";
		final String citeTextMatchPre = "(<ac:rich-text-body>|<ac:plain-text-body>)";
		final String citeTextMatchPost = "(</ac:rich-text-body>|</ac:plain-text-body>)";
		
		SecurityContextHolder.setContext(securityContext);

		// prepare a new cite map
		LinkedHashMap<String, CiteData> citeMap = new LinkedHashMap<String, CiteData>();
		
		// get the content of the page
		String content = "";
		if(presentContent != null) {
			content = presentContent;
		}
		else {
			ContentAndVersion contentAndVersion = new ContentAndVersion();
			contentAndVersion = getPageContentAndVersion(cCLink, pageId);
			content = contentAndVersion.content;
			pageVersion = contentAndVersion.version;
		}
		
		BalancedMatcher bm = new BalancedMatcher(content, "ac:name=(\\\"single-cite\\\"|\\\"short-cite\\\"|\\\"single-cite-short\\\"|\\\"single-cite-import\\\"|\\\"single-cite-plain\\\"|\\\"bibtex-cite\\\"|\\\"dbibtex-cite\\\"|\\\"doi-cite\\\")", "<ac:structured-macro", "</ac:structured-macro", -1);
		
		Date startDate = new Date();
		
		while(bm.find()) {
						
			// get macro-id, cite-id and cite text
			String matchTruncated = bm.getMatch().replaceFirst("<ac:structured-macro.*", "");
			String matchMacroID = getMiddlePart(matchTruncated, macroIDMatchPre, macroIDMatchPost, false);
			String matchCiteID = getParameter(matchTruncated, "citeID");
			String citeText = getMiddlePart(bm.getMatch(), citeTextMatchPre, citeTextMatchPost, true).replaceAll("^<p.*?>", "").replaceAll("<\\/p>$", "");
			String importFrom = getParameter(matchTruncated, "importFrom");
			String compiledCite = getParameter(matchTruncated, "compiledCite");
			String addedLink = getParameter(matchTruncated, "addedLink");
			boolean defineOnly = getParameter(matchTruncated, "defineOnly").equals("true");
            boolean addLink = getParameter(matchTruncated, "addLink").equals("true"); // legacy
			
			// in case we already have that cite
			if(citeMap.containsKey(matchCiteID)) {
				CiteData cite = citeMap.get(matchCiteID);
				if(!defineOnly) {
					cite.addMacroID(matchMacroID);
				}
				if(!citeText.isEmpty()) {
					cite.setCiteText(citeText);
				}
				citeMap.put(matchCiteID, cite);
			}
			
			// in case the cite is new
			else {
				CiteData cite = new CiteData();
				if(!defineOnly) {
					cite.addMacroID(matchMacroID);
				}
				cite.setNumber(citeMap.size() + 1);
				
				// imported cite
				if(!importFrom.isEmpty()) {
					cite.setCiteText(importIdent + importFrom);
				}
				
				// compiled cite (DOI, BibTeX or new plain (basic) cite)
				else if (!compiledCite.isEmpty() && !compiledCite.equals("undefined")) {
					
					// unescape the compiled cite and allow some basic HTML
					compiledCite = HtmlUtils.htmlUnescape(compiledCite).replaceAll("^<p.*?>", "").replaceAll("<\\/p>$", "");
					compiledCite = Jsoup.clean(compiledCite, Safelist.basic().removeEnforcedAttribute("a", "rel").addAttributes("span", "style"));
					
                    if(addLink) { // legacy
                        cite.setCiteText("<a href='http://dx.doi.org/" + matchCiteID + "'>"+ compiledCite + "</a>");
                    }
					else if(addedLink.isEmpty()) {
						cite.setCiteText(compiledCite);
					}
					else {
						cite.setCiteText("<a href='" + HtmlUtils.htmlUnescape(addedLink) + "'>" + compiledCite + "</a>");
					}
				}
				
				// regular cite
				else {					
					if(addedLink.isEmpty()) {
						cite.setCiteText(citeText);
					}
					else {
						cite.setCiteText("<a href='" + HtmlUtils.htmlUnescape(addedLink) + "'>" + citeText + "</a>");
					}
				}
				
				citeMap.put(matchCiteID, cite);			
			}			
		}
		
		System.out.printf("INFO: preparing the cite map for %s and %s took: %d%n", cCLink, pageId, new Date().getTime() - startDate.getTime());
		
		// and return it
		return citeMap;
	}
	
	
	// generate the cache key
	private String generateCacheKey(String cCLink, String pageId, String pageVersion) {
		return cCLink + ";" + pageId + ";" + pageVersion;
	}
	
	
	// get the page data and its content
	private ContentAndVersion getPageContentAndVersion(String baseUrl, String pageId) {
		
		ContentAndVersion contentAndVersion = new ContentAndVersion();

		// try to get the page content via REST
		try {
			System.out.printf("INFO: %s: getting page content of: %s%n", baseUrl, pageId);
				
			// make REST call and extract the data out of the answer
			ResponseEntity<String> responseEF = getStringEntitySafe(baseUrl + "/api/v2/pages/" + pageId + "?body-format=storage");
	
			JSONObject response = new JSONObject(responseEF.getBody());
					
			contentAndVersion.content = response.getJSONObject("body").getJSONObject("storage").getString("value");
			contentAndVersion.version = Integer.toString(response.getJSONObject("version").getInt("number"));
			
			// return the content
			return contentAndVersion;
		}
		
		// in case of an exception
		catch (Exception e) {
			System.out.printf("ERROR: %s: could not get data of page %s: %s%n", baseUrl, pageId, e.toString());
			//System.out.println(getStackTrace(e));
			return contentAndVersion;
		}
	}
	
	
	// perform a query to get all pages with cites in it
	private String searchCites(String cCLink, String spaces, String currentSpace) {
		
		
		// try to perform the search via rest
		try {
			System.out.printf("INFO: %s: searching cites%n", cCLink);
			
			// make a search REST call
			String spaceSearch = spaces.isEmpty() ? currentSpace : spaces;
			ResponseEntity<String> responseE = getStringEntitySafe(cCLink + "/rest/api/content/search?cql=(macro IN ('single-cite','single-cite-import','doi-cite','bibtex-cite','single-cite-plain'))AND(space IN (" + spaceSearch + "))&expand=body.storage,version,space&limit=1000");

			// return the content
			return responseE.getBody();
		}
		
		// in case of an exception
		catch (Exception e) {
			System.out.printf("ERROR: %s: could not search cites: %s%n", cCLink, e.toString());
			return "";
		}
	}
	

	// load content as string either as user or as add-on in case the first one fails
	private ResponseEntity<String> getStringEntitySafe(String url) {
		try {
			return atlassianHostRestClients.authenticatedAsHostActor().getForEntity(url, String.class);		
		}
		catch(Exception e1) {
			System.out.printf("WARN: could not receive as host user %s: %s%n", url, e1.toString());
			try {
				return atlassianHostRestClients.authenticatedAsAddon().getForEntity(url, String.class);
			}
			catch(Exception e2) {
				System.out.printf("ERROR: could not receive %s: %s%n", url, e2.toString());
				return new ResponseEntity<String>(HttpStatus.BAD_REQUEST);
			}
		}
	}
		
	
	// return the message in case of a license failure
	public String getNoLicenseOutput() {
		return messageSource.getMessage("simple-cite-cloud.message.noLicense", null, Locale.getDefault());
	}
	
	
	// extract the middle between two regex expressions
	private String getMiddlePart(String str, String pre, String post, boolean maxRange) {
		Pattern p = maxRange ? Pattern.compile(pre.concat(".*").concat(post), Pattern.DOTALL) : Pattern.compile(pre.concat(".*?").concat(post), Pattern.DOTALL);
		Matcher m = p.matcher(str);
		
		if(m.find()) {
			String match = m.group(0);
			return match.replaceAll(pre, "").replaceAll(post, "");
		}
		else {
			return "";
		}
	}
	
	
	// get a parameter out of the storage format of a macro
	private String getParameter(String storage, String name) {
		final String matchPre = "ac:name=\"" + name + "\">";
		final String matchPost = "</ac:parameter>";
		return getMiddlePart(storage, matchPre, matchPost, false);
	}
	
	
	// print the stack trace of an exception
	private static String getStackTrace(final Throwable throwable) {
	     final StringWriter sw = new StringWriter();
	     final PrintWriter pw = new PrintWriter(sw, true);
	     throwable.printStackTrace(pw);
	     return sw.getBuffer().toString();
	}
}
