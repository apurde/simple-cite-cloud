package de.edrup.confluence.plugins.simplecite.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class BalancedMatcher {
	
	private int start;
	private int end;
	private String searchIn;
	private String startRegEx;
	private String incRegEx;
	private String decRegEx;
	private int target;
	
	
	public BalancedMatcher(String searchIn, String startRegEx, String incRegEx, String decRegEx, int target) {
		this.searchIn = searchIn;
		this.startRegEx = startRegEx;
		this.incRegEx = incRegEx;
		this.decRegEx = decRegEx;
		this.target = target;
		this.start = -1;
	}
	
	
	public boolean find() {
		return find(start + 1);
	}
	
	
	public boolean find(int startIndex) {
		Pattern startPattern = Pattern.compile(startRegEx, Pattern.DOTALL);
		Pattern incPattern = Pattern.compile(incRegEx, Pattern.DOTALL);
		Pattern decPattern = Pattern.compile(decRegEx, Pattern.DOTALL);
		Matcher startMatcher = startPattern.matcher(searchIn);
		int balance = 0;
		if(startMatcher.find(startIndex)) {
			int currentOffset = 0;
			int decPos = -1;
			int incPos = -1;
			Matcher incMatcher = incPattern.matcher(searchIn.substring(startMatcher.end()));
			Matcher decMatcher = decPattern.matcher(searchIn.substring(startMatcher.end()));
			do {
				incPos = incMatcher.find(currentOffset) ? incMatcher.start() : Integer.MAX_VALUE;
				decPos = decMatcher.find(currentOffset) ? decMatcher.start() : Integer.MAX_VALUE;
				if(incPos < decPos) {
					balance++;
					currentOffset = incMatcher.end();
				}
				if(decPos < incPos) {
					balance--;
					currentOffset = decMatcher.end();
				}
			}
			while((balance != target) && ((decPos != Integer.MAX_VALUE ) || (incPos != Integer.MAX_VALUE)));
			start = startMatcher.start();
			end = startMatcher.end() + currentOffset;
			return true;
		}
		else {
			return false;
		}
	}
	
	
	public String getMatch() {
		return searchIn.substring(start, end);
	}
	
	
	public int start() {
		return start;
	}
	
	
	public int end() {
		return end;
	}
}

